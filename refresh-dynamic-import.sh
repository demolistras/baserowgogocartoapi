




curl 'https://demolistras.gogocarto.fr/admin/app/importdynamic/9/refresh' \
	  -H 'authority: demolistras.gogocarto.fr' \
	    -H 'sec-ch-ua: "Chromium";v="91", " Not;A Brand";v="99"' \
	      -H 'sec-ch-ua-mobile: ?1' \
	        -H 'upgrade-insecure-requests: 1' \
		  -H 'user-agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Mobile Safari/537.36' \
		    -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
		      -H 'sec-fetch-site: same-origin' \
		        -H 'sec-fetch-mode: navigate' \
			  -H 'sec-fetch-user: ?1' \
			    -H 'sec-fetch-dest: document' \
			      -H 'referer: https://demolistras.gogocarto.fr/admin/app/importdynamic/list' \
			        -H 'accept-language: en-US,en;q=0.9,fr;q=0.8' \
				  -H 'cookie: _pk_id.13.7078=04e79d5e009d2319.1611736470.; firstVisit=done; _pk_ses.13.7078=1; viewport=@47.86,8.69,6z; address=; PHPSESSID=5rnnftqih9i55jgfnk5lrcgdn1' \
				    --compressed
