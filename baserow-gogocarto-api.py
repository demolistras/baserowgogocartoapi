




from requests import get
from json import loads, dump

baserow_apikey = open("/home/ubuntu/secrets/baserow.scenariob.org_apikey", 'r').read()[:-1]

base_url = "https://baserowapi.scenariob.org/api/database/rows/table/50/?user_field_names=true"
headers = {
          "Authorization": "Token {}".format(baserow_apikey)
        }

input_data = loads(get(base_url,headers=headers).content)


output_data = [
          {
              "id": "ABC123",
              "name": input_data["results"][0]["Ville"][0]["value"],
              "geo": {"latitude":45.77745,"longitude":3.08194},
          }
        ]

#{"id":"D1","name":"Eco-colocation Massif-Central","geo":{"latitude":45.77745,"longitude":3.08194},"sourceKey":"demo gitlab","address":{ "addressLocality":"Clermont-Ferrand", "addressCountry":"fr"},"createdAt":"2021-05-25T11:15:22+02:00","updatedAt":"2021-05-25T11:15:22+02:00","status":7,"categories": ["Groupe local"],"categoriesFull": [{"id":9, "name":"Groupe local", "description":"", "index":0}]},{"id":"D2","name":"Eco-colocation Amiens","geo":{"latitude":49.89417,"longitude":2.29569},"sourceKey":"demo gitlab","address":{ "addressLocality":"Amiens", "addressCountry":"fr"},"createdAt":"2021-05-25T11:15:22+02:00","updatedAt":"2021-05-25T11:15:22+02:00","status":7,"categories": ["Groupe local"],"categoriesFull": [{"id":9, "name":"Groupe local", "description":"", "index":0}]},{"id":"D3","name":"Eco-colocation Grand-Est","geo":{"latitude":48.69372,"longitude":6.1834},"sourceKey":"demo gitlab","address":{ "addressLocality":"Nancy", "addressCountry":"fr"},"createdAt":"2021-05-25T11:15:22+02:00","updatedAt":"2021-05-25T11:15:22+02:00","status":7,"categories": ["Groupe local"],"categoriesFull": [{"id":9, "name":"Groupe local", "description":"", "index":0}]},{"id":"D4","name":"Eco-colocs","geo":{"latitude":48.1371,"longitude":11.57538},"sourceKey":"demo gitlab","address":{ "addressLocality":"Munich", "addressCountry":"de"},"createdAt":"2021-05-25T11:15:22+02:00","updatedAt":"2021-05-25T11:15:22+02:00","status":7,"categories": ["ap\u00e9ro souhait\u00e9"],"categoriesFull": [{"id":12, "name":"ap\u00e9ro souhait\u00e9", "description":"", "index":0}]},{"id":"D5","name":"Quartier Gare Forever","geo":{"latitude":48.58407,"longitude":7.73604},"sourceKey":"demo gitlab","address":{ "addressLocality":"Strasbourg", "addressCountry":"fr"},"createdAt":"2021-05-25T11:15:22+02:00","updatedAt":"2021-05-25T11:15:22+02:00","status":7,"categories": ["\u00c9co-colocation install\u00e9e"],"categoriesFull": [{"id":1, "name":"\u00c9co-colocation install\u00e9e", "description":"", "index":0}]},{"id":"D6","name":"Colocation des costi\u00e8res de N\u00eemes","geo":{"latitude":43.79429,"longitude":4.53117},"sourceKey":"demo gitlab","address":{ "addressLocality":"Bellegarde", "postalCode":"30127", "addressCountry":"fr"},"createdAt":"2021-05-25T11:15:22+02:00","updatedAt":"2021-05-25T11:15:22+02:00","status":7,"categories": ["\u00c9co-colocation install\u00e9e"],"categoriesFull": [{"id":1, "name":"\u00c9co-colocation install\u00e9e", "description":"", "index":0}]},{"id":"D7","name":"L' \"\u00e9co-quelicots\"","geo":{"latitude":47.38155,"longitude":0.6739},"sourceKey":"demo gitlab","address":{"streetAddress":"Rue du Capitaine Pougnon", "addressLocality":"Tours", "postalCode":"37032", "addressCountry":"fr"},"createdAt":"2021-05-25T11:15:22+02:00","updatedAt":"2021-05-25T11:15:22+02:00","status":7,"categories": ["\u00c9co-colocation install\u00e9e"],"categoriesFull": [{"id":1, "name":"\u00c9co-colocation install\u00e9e", "description":"", "index":0}]},{"id":"D8","name":"Eco-coloc Villeneuve-d'Ascq","geo":{"latitude":50.63042,"longitude":3.13123},"sourceKey":"demo gitlab","address":{"streetAddress":"Rue de Fives", "addressLocality":"Villeneuve-d'Ascq", "postalCode":"59652", "addressCountry":"fr"},"createdAt":"2021-05-25T11:15:22+02:00","updatedAt":"2021-05-25T11:15:22+02:00","status":7,"categories": ["\u00c9co-colocation install\u00e9e"],"categoriesFull": [{"id":1, "name":"\u00c9co-colocation install\u00e9e", "description":"", "index":0}]},{"id":"D9","name":"Vitry de la paix","geo":{"latitude":48.79268,"longitude":2.40404},"sourceKey":"demo gitlab","address":{"streetAddress":"Rue de la Paix", "addressLocality":"Vitry-sur-Seine", "postalCode":"94400",

api_compliant = {
                "licence": "https://opendatacommons.org/licenses/odbl/summary/",
                "ontology":"gogofull",
                "data":output_data}

with open("api/elements.json", 'w') as f:
    dump(api_compliant, f)
